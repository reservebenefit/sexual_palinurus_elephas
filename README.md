## Analysis

* We want to check if some selected SNPs linked to sexual characters on *Palinurus elephas* are located on genes.
* We use the genome of *Palinurus ornatus* as reference.



1. Download *Palinurus ornatus* genome and annotation

```
bash download_genomes.sh
```

2. Blast sequences against genome


```
bash align_sequences_against_reference.sh
```

Keep only records with:

* identity pourcentage alignment > 0.85
* Length of alignment > 31 pb
* E-value < 10^-4

```
Rscript filter_blastn_output.R
```

3. Find genes

With the coordinates of the alignment onto genomes, return the genes located at these coordinates.

```
bash get_genes.sh
```