## fasta genome
wget https://zenodo.org/record/4589425/files/Panulirus.ornatus.genome.fasta.gz?download=1 -O palinurus_ornatus_genome.fasta.gz
## gff3 genome
wget https://zenodo.org/record/4589425/files/Panulirus.ornatus.genome.gff.gz?download=1 -O palinurus_ornatus_genome.gff.gz

gunzip -d palinurus_ornatus_genome.fasta.gz
gunzip -d palinurus_ornatus_genome.gff.gz