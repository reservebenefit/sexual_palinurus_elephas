makeblastdb -dbtype nucl -in palinurus_ornatus_genome.fasta -parse_seqids -title "genome palinurus ornatus"

blastn -query 277sequences.fasta \
-db ${PWD}"/"palinurus_ornatus_genome.fasta \
-task blastn -outfmt "7 delim=, qacc sacc evalue bitscore qcovus pident length qstart qend" -max_target_seqs 5 > blastn.out

HEADER="qacc,sacc,evalue,bitscore,qcovus,pident,length,qstart,qend"
cat <(echo ${HEADER}) <(grep -v "^#" blastn.out) > blastn.csv

# filter results with at least
# pident > 0.85
# evalue < 10^-4
# length > 31